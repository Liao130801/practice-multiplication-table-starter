package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if(!isValid(start,end)) return null;
        return generateTable(start,end);
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start)&&isInRange(end)&&isStartNotBiggerThanEnd(start,end);
    }

    public Boolean isInRange(int number) {
        return number > 1 && number < 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        if(start<=end)return true;
        return false;
    }

    public String generateTable(int start, int end) {
        StringBuilder multiplicationTable = new StringBuilder();
        for (int i = start; i <= end; i++) {
            multiplicationTable.append(generateLine(start,i));
            String format =String.format("%n");
            if(i<end) multiplicationTable.append(format);
        }
        return multiplicationTable.toString();
    }

    public String generateLine(int start, int row) {
        StringBuilder buildRow = new StringBuilder();
        for (int j = start; j <= row; j++) {
            buildRow.append(generateSingleExpression(row,j));
            if(j<row)buildRow.append("  ");
        }
        return buildRow.toString();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        StringBuilder buildExpress = new StringBuilder();
        return buildExpress.append(multiplier).append("*").append(multiplicand).append("=").append(multiplicand * multiplier).toString();
    }
}
